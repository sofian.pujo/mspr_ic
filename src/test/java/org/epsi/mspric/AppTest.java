package org.epsi.mspric;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest 
    extends TestCase
{

    public void testDivide()
    {
        assertEquals(4.0, App.divide(8, 2));
        assertEquals(0.0, App.divide(8, 0));
    }
}
