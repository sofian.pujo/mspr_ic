package org.epsi.mspric;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class App {

    private static Logger logger = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        logger.info("8 / 2 = {}", divide(8, 2));
    }

    public static double divide(double numerator, double denominator) {
        if (denominator != 0) {
            return numerator / denominator;
        } else {
            return 0;
        }
    }
}